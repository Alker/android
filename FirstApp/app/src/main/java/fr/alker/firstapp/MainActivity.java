package fr.alker.firstapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private final String defaut = "Vous devez cliquer sur le bouton ''Calculer l'IMC'' pour obtenir un résultat.";
    private final String megaString = "T'es gros.";

    Button envoyer = null;
    Button raz = null;

    EditText poids = null;
    EditText taille = null;

    RadioGroup group = null;

    TextView result = null;

    CheckBox mega = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //On recupere les vues dont on a besoin
        envoyer = (Button) findViewById(R.id.BoutonIMC);
        raz = (Button) findViewById(R.id.BoutonRAZ);

        taille = (EditText) findViewById(R.id.taille);
        poids = (EditText) findViewById(R.id.poids);

        mega = (CheckBox) findViewById(R.id.mega);

        group = (RadioGroup) findViewById(R.id.group);

        result = (TextView) findViewById(R.id.result);

        //On attribue un listener adapté aux vues qui en ont besoin
        envoyer.setOnClickListener(envoyerListener);
        raz.setOnClickListener(razListerner);
        taille.addTextChangedListener(textWatcher);
        poids.addTextChangedListener(textWatcher);
        mega.setOnClickListener(checkedListener);
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            result.setText(defaut);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private OnClickListener envoyerListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            if(!mega.isChecked()){
                //si la megafonction n'est pas activée
                //on recupere la taille et poids
                String t = taille.getText().toString();
                String p = poids.getText().toString();

                float tValue = Float.valueOf(t);

                //on verifie que la taille est cohérente

                if(tValue == 0)
                    Toast.makeText(MainActivity.this,"T'as cru, arrete de faire le con!",Toast.LENGTH_SHORT).show();
                else{
                    float pValue = Float.valueOf(p);
                    //on verifie si la taille est en metre ou en centimetre
                    if(group.getCheckedRadioButtonId() == R.id.centi)
                        tValue = tValue/100;
                    tValue = (float)Math.pow(tValue,2);
                    float imc = pValue / tValue;
                    result.setText("Votre IMC est " + String.valueOf(imc));
                }
            }else result.setText(megaString);
        }
    };

    //listener bouton raz
    private OnClickListener razListerner = new OnClickListener() {
        @Override
        public void onClick(View v) {
            poids.getText().clear();
            taille.getText().clear();
            result.setText(defaut);
        }
    };

    private OnClickListener checkedListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            //on remet le texte par defaut si c'etait le text de la megafonction
            if(!((CheckBox)v).isChecked() && result.getText().equals(megaString))
                result.setText(defaut);
        }
    };
}
