package fr.alker.nbaapp;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.*;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button Jordan = null;
    Button Kobe = null;
    Button ONeil = null;
    Button Wade = null;

    TextView result = null;

    ImageView img = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    Jordan = (Button)findViewById(R.id.Jordan);
    Kobe = (Button)findViewById(R.id.Kobe);
    ONeil = (Button)findViewById(R.id.shaq);
    Wade = (Button)findViewById(R.id.wade);
    result = (TextView)findViewById(R.id.desc);
    img = (ImageView)findViewById(R.id.image);

    Jordan.setOnClickListener(jordanListener);
    Kobe.setOnClickListener(KobeListener);
    ONeil.setOnClickListener(OnielListener);
    Wade.setOnClickListener(WadeListener);
    result.addTextChangedListener(descListener);
    }

    private TextWatcher descListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private OnClickListener jordanListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
                result.setText("Michael Jordan");
                img.setImageResource(R.drawable.a1);
        }
    };

    private OnClickListener KobeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            result.setText("Kobe Bryant");
            img.setImageResource(R.drawable.b2);
        }
    };

    private OnClickListener OnielListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            result.setText("Shaquille O'Neil");
            img.setImageResource(R.drawable.c3);
        }
    };

    private OnClickListener WadeListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            result.setText("Dwyane Wade");
            img.setImageResource(R.drawable.d4);
        }
    };
}
