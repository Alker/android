package fr.alker.tp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //On récupère le bouton par son identifiant
        Button b = (Button) findViewById(R.id.bouton);
        //On lui indique que cette classe sera son listener pour l'évènement Touch
        b.setOnTouchListener(this);
    }

    //Fonction qui sera lancé à chaque fois qu'un toucher est détecté sur le
    // bouton rattaché

    @Override
    public boolean onTouch(View view, MotionEvent event){
        //L'évènement nous donne la vue concernée par le toucher, on
        // le récupère et on le caste en bouton
        Button bouton = (Button)view;

        //On recupere la largeur du bouton
        int larg = bouton.getWidth();
        //on recupere la hauteur du bouton
        int haut = bouton.getHeight();

        //on recupère a la coordonnée x y de l'évènement
        float x = event.getX();
        float y = event.getY();

        //puis on change la taille
        bouton.setTextSize(Math.abs(x-larg/2) + Math.abs(y-haut/2));
        //le toucher est fini, on veut continuer à detecter les toucher aprés
        return true;
    }
}
